---
##Slovensko
---

###GIT2FTPDeploy z Hipchat obvestili
**Opomnik:** To je dejansko kopija od [Wanchai's FTPBucket na Github-u](https://github.com/Wanchai/FTPbucket) tako da je večinoma njegovo delo. Za uporabo beri spodaj v Ang. **FTPbucket**

Kar je narejeno iz moje strani je:

- v config.php datoteki, dodano za Hipchat nastavitve
- v FTPbucket.php, dodano Hipchat API preko katerega se obvešča o osnovnih stvareh. Vse kar boste dobili v obvestilo je to kaj je bilo spremenjeno in ali je bil prenos uspešen ter z datumom in časom.

**_Na takšen način_**		
![Hipchat obvestila](http://up.imamo.si/HipChat_-_FTPloy.png)

#### Zaznamki v datoteke dnevnika namesto v HipChat obvestlo (kot je v orig. skripti)
Če ne želite prejemati HipChat obvestil potem lahko tudi nastavite da vam log skripta zapisuje v datoteke. Tako je tudi v orig. skripti nastavljo. Vendar pa morate spremeniti naslednje vrstice

>odkomentiraj vrstice v **FTPbucket.php**	
	- line 24		
	- line 35		
	- from line 184 to 186		
	- and from line 192 to 194

>zakomentiraj vrstice v **FTPbucket.php**	
	- line 183	
	- line 191

Ravno tako pa boste verjetno želeli nastaviti geslo za dostop do dnevnika preko spleta v **config.php datoteki, vrstica 41** 
>'admin_pass' => 'yourpassword'		
	

---	
##ENGLISH
---	

###GIT2FTPDeploy with Hipchat notifications
**Disclaimer:** This is actually fork of [Wanchai's FTPBucket on Github](https://github.com/Wanchai/FTPbucket) so 99% of the code is his. I just changed next things for simple hipchat notifications (logs) which I prefer instead log files :

- config.php - for hipchat settings
- FTPbucket.php - added hipchat API call for really basic notifications/log. All what you get in notification is  changed/uploaded file with date and time.

*__Something like that:__*		
![Hipchat notification](http://up.imamo.si/HipChat_-_FTPloy.png)


#### Log files instead Hipchat notifications (as it is in original script)
If you don't want HipChat notifications you could also have log file as it is in original script. But you have to 

>comment out next lines in **FTPbucket.php**	
	- line 24		
	- line 35		
	- from line 184 to 186		
	- and from line 192 to 194

>and comment next lines in **FTPbucket.php**	
	- line 183	
	- line 191

Also you would probably want to set password for web access in **config.php file, line 41** 
>'admin_pass' => 'yourpassword'	
	
---	
	
		
*__(Original README from Whachai - recommended reading)__*
FTPbucket
=========

FTPbucket is a PHP script that enables you to sync your BitBucket repository with any FTP account.

INSTALLATION
------------

- Edit the config file and rename it to 'config.php'
- Copy the deploy folder on your FTP server
- On Bitbucket>Admin>Hooks, setup a POST hook pointing to http://myserver/deploy/deploy.php
 
LOGS
-----
You can see and clear the logs by connecting to http://myserver/deploy/ 
You have to setup a password in the config file.

LIMITATIONS
-----------

1. The script only copies the files you are pushing. It means that if you start with this tool when you already have files in your Bitbucket repo, they won't be copied on the server. I'm looking for solutions on a full deploy. Which brings me the second point.
2. I tried to push a 160Mo repo with more than 26 000 files and the POST hook didn't like it. The limit is 1000 files/push I think. It's an unsolved issue: https://bitbucket.org/site/master/issue/7439/git-post-commit-hook-payloads-has-empty

SOLUTION : When you create a new repo on BB and need to push a lot of files, just do it. Right after, you set up the POST hook and manually copy the repo and FTPbucket files on your FTP.

MORE
----

It should work with Mercurial too but it's not tested yet.

I'm sure a lot of improvements can be made to my code so don't hesitate to fork and improve it! I would be glad to hear about your tests and issues too.

TODO
----

- Add a post-commit hook
- Add support for SSH, Github...

LICENCE
-------
Copyright (c) 2014 [Thomas Malicet](http://www.thomasmalicet.com/)

This code is an open-sourced software licensed under the [BEER-WARE LICENCE] (https://fedoraproject.org/wiki/Licensing/Beerware)